/** Bài 1
 * Input: Lương 100k/1ngày
 * Step:
 * s1: Tạo 2 biến chứa: lương 1 ngày, số ngày làm
 * s2: Áp dụng công thức tính lương: số ngày làm*lương 1 ngày 
 * Output: Tổng lương
 */
var luong1ngay = 100;
var ngaylam =5;
var luong=null;
luong= luong1ngay * ngaylam;
console.log("LƯƠNG:",luong);

/**Bài 2
 * Input: Cho 5 số thực
 * Step:
 * s1:Tạo 5 biến chứa 5 số thực
 * s2:Áp dụng Công thức tính giá trị trung bình: (Tổng 5 số thực)/5
 * Output: Giá trị trung bình
 */
var age1=1;
var age2=2;
var age3=3;
var age4=4;
var age5=5;
var GiatriTB=null;
GiatriTB=(age1+age2+age3+age4+age5)/5;
console.log("GIÁ TRỊ TRUNG BÌNH:",GiatriTB,);

/**Bài 3
 * Input: 1 USD = 23.500 VND
 * Step: 
 * s1: Tạo 2 biến chứa: giá USD hiện nay, số tiền cần đổi
 * s2: Công thức quy đổi: 23.500 * số tiền cần đổi 
 * Output: Số tiền sau quy đổi VND
 */
var usd=23.5;
var sotiencandoi= 50;
var Total=null;
Total= usd * sotiencandoi;
console.log("Total:",Total,"VND");

/**Bài 4
 * Input: Chiều dài và chiều rộng
 * Step: 
 * s1: Tạo ra 2 biến chứa: Chiều dài và chiều rộng
 * s2: Áp dụng công thức tính chu vi và diện tích hình chữ nhật
 * Output: Kết quả: Diện tích, chu vi 
 */
var chieudai=5;
var chieurong=10;
var chuvi=null;
var dientich=null;
chuvi=(chieudai + chieurong) * 2;
dientich=chieudai * chieurong;
console.log("CHU VI:",chuvi);
console.log("DIỆN TÍCH:",dientich);

/** Bài 5
 * Input: số có 2 chữ số
 *
 * Step:
 * 1. tạo ra 2 biến số của hàng đơn vị, hàng chục
 * 2. tính các hàng đơn vị, chục
 *
 *
 * Output: tổng 2 số
 */
var num = 36;
var hangDonVi, hangChuc, Tong;
hangDonVi = num % 10;
hangChuc = Math.floor((num % 100) / 10);
Tong = hangDonVi + hangChuc;
console.log("TỔNG 2 KÝ SỐ LÀ: ", Tong);
